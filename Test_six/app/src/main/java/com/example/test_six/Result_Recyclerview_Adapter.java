package com.example.test_six;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.litepal.LitePal;

import java.util.List;

/**
 * Created by czm
 */

public class Result_Recyclerview_Adapter extends RecyclerView.Adapter<Result_Recyclerview_Adapter.ViewHolder> {
    private static final String TAG = "Result_Recyclerview_Ada";
    private List<Book> select_results;
    private Context context;

    public Result_Recyclerview_Adapter(List<Book> select_results) {
        Log.i(TAG, "Result_Recyclerview_Adapter: 构造函数");
        this.select_results = select_results;
    }

    /**
     * 创建viewhodler
     *
     * @param parent   调用的activity
     * @param viewType
     * @return 组装好的viewhodler
     */
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: 获得context");
        this.context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_result_recyclerview_item, parent, false);
        Log.i(TAG, "onCreateViewHolder: parent.getContext :" + parent.getContext().toString());
        Log.d(TAG, "onCreateViewHolder: 组装 viewholder");
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    /**
     * 获取书名
     *
     * @param holder   viewholer是之前组装的viewholder
     * @param position 书在list中的排序
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Book book = select_results.get(position);
        holder.book_name.setText(book.getName());
        Log.i(TAG, "onBindViewHolder: book_name =" + book.getName());
        /*删除按钮的响应*/
        holder.book_declect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i(TAG, "onClick: 确定");
                        LitePal.delete(Book.class, select_results.get(position).getId());
                        Log.i(TAG, "onClick: delected_book =" + select_results.get(position).toString());
                        select_results.remove(position);
                        notifyDataSetChanged(); // 刷新
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i(TAG, "onClick: 取消");
                    }
                });
                builder.setTitle("警告");
                builder.setMessage("是否删除");
                builder.show();
            }
        });
        /*修改按钮的响应*/
        holder.book_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: book_change");
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.add_book_data_dailog);
                TextView title = dialog.findViewById(R.id.book_data_title);
                title.setText("修改数据");
                EditText book_name = (EditText) dialog.findViewById(R.id.get_book_name);
                EditText book_author = (EditText) dialog.findViewById(R.id.get_book_author);
                EditText book_pages = (EditText) dialog.findViewById(R.id.get_book_pages);
                EditText book_price = (EditText) dialog.findViewById(R.id.get_book_price);
                EditText book_press = (EditText) dialog.findViewById(R.id.get_book_Press);
                String price = String.valueOf(select_results.get(position).getPrice());
                String pages = String.valueOf(select_results.get(position).getPages());
                String name = select_results.get(position).getName();
                String author = select_results.get(position).getAuthor();
                final int id = select_results.get(position).getId();
                book_name.setText(name);
                book_author.setText(author);
                book_pages.setText(pages);
                book_price.setText(price);
                book_press.setText(select_results.get(position).getPress());
                Button edit_data_ok = dialog.findViewById(R.id.edit_data_ok);
                edit_data_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i(TAG, "onClick: edit_data_ok");
                        Book new_book = new Book();
                        EditText book_name = (EditText) dialog.findViewById(R.id.get_book_name);
                        EditText book_author = (EditText) dialog.findViewById(R.id.get_book_author);
                        EditText book_pages = (EditText) dialog.findViewById(R.id.get_book_pages);
                        EditText book_price = (EditText) dialog.findViewById(R.id.get_book_price);
                        EditText book_press = (EditText) dialog.findViewById(R.id.get_book_Press);
                        new_book.setId(id);
                        new_book.setName(book_name.getText().toString().trim());
                        new_book.setAuthor(book_author.getText().toString().trim());
                        new_book.setPages(Integer.valueOf(book_pages.getText().toString().trim()));
                        new_book.setPrice(Double.parseDouble(book_price.getText().toString().trim()));
                        new_book.setPress(book_press.getText().toString().trim());
                        Log.i(TAG, "onClick: edit_data_ok: new_book =" + new_book.toString());
                        new_book.update(id);
                        select_results.set(position, new_book);
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                Button edit_data_cancel = dialog.findViewById(R.id.edit_data_cancel);
                edit_data_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i(TAG, "onClick: edit_data_ok: cancel");
                        dialog.dismiss();
                    }
                });
                Log.d(TAG, "onClick: dialog show");
                dialog.show();

            }
        });
    }

    /**
     * 获取list长度
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return select_results.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView book_name;
        Button book_declect;
        Button book_change;

        public ViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "ViewHolder: 初始化各种组件");
            book_name = (TextView) itemView.findViewById(R.id.result_book_name);
            book_declect = (Button) itemView.findViewById(R.id.book_delect);
            book_change = (Button) itemView.findViewById(R.id.book_change);
        }
    }
}

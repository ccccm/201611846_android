package com.example.test_six;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.litepal.LitePal;

import java.util.List;

public class Select_Result_Activity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "Select_Result_Activity";
    List<Book> bookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_result);
        Log.i(TAG, "onCreate: onCreate");
        Intent intent = getIntent();
        Log.i(TAG, "onCreate: id =" + intent.getStringExtra("id"));
        bookList = get_result_list(Integer.valueOf(intent.getStringExtra("id")));
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.select_result_recyclerview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        Result_Recyclerview_Adapter adapter = new Result_Recyclerview_Adapter(bookList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        String message = "响应失败";
        switch (view.getId()) {
            case R.id.to_back:
                message = "返回";
                Log.i(TAG, "onClick: to_back");
                finish();
                break;
            default:
                message = "响应失败";
                Log.i(TAG, "onClick: default");
                break;
        }
        Toast.makeText(Select_Result_Activity.this, message, Toast.LENGTH_SHORT).show();
    }

    public List<Book> get_result_list(int id) {
        List<Book> books = null;
        if (id!=0) {
            books.add(LitePal.find(Book.class, id));
        } else {
            books = LitePal.findAll(Book.class);
        }
        return books;
    }
}


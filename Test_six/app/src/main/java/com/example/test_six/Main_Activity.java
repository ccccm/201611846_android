package com.example.test_six;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.litepal.LitePal;

//Litepal 2.0  参考网址https://blog.csdn.net/c10WTiybQ1Ye3/article/details/80649776
public class Main_Activity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: oncreate");
        Log.d(TAG, "onCreate: new dialog");
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.add_book_data_dailog);
        Log.i(TAG, "onCreate: " + dialog.toString());
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick: onClick");
        TextView title = (TextView) dialog.findViewById(R.id.book_data_title);
        title.setText("添加信息");
        String message = new String("响应失败");
        switch (view.getId()) {
            case R.id.create_database:
                message = "创建成功";
                Log.i(TAG, "onClick: 创建数据库");
                LitePal.getDatabase();  //进行一次任意数据库操作用于创建数据库
                break;
            case R.id.add_data:
                message = "添加成功";
                Log.i(TAG, "onClick: 添加固定数据");
                Book book = new Book();
                book.setName("The Da Vinci Code");
                book.setAuthor("Dan Brown");
                book.setPages(454);
                book.setPrice(16.96);
                book.setPress("Unknow");
                book.save();
                Log.i(TAG, "onClick: book = " + book.toString());
                break;
            case R.id.delete_data:
                Log.i(TAG, "onClick: delete_data");
                int result = LitePal.deleteAll(Book.class);
                Log.i(TAG, "onClick: data_detered_result = " + result);
                message = "成功删除";
                break;
            case R.id.add_data_write:
                Log.i(TAG, "onClick: add_data_write");
                message = "添加数据";
                dialog.show();
                break;
            case R.id.edit_data_ok:
                Log.i(TAG, "onClick: edit_data_ok");
                Log.d(TAG, "onClick: 组装book");
                Book new_book = new Book();
                EditText book_name = (EditText) dialog.findViewById(R.id.get_book_name);
                EditText book_author = (EditText) dialog.findViewById(R.id.get_book_author);
                EditText book_pages = (EditText) dialog.findViewById(R.id.get_book_pages);
                EditText book_price = (EditText) dialog.findViewById(R.id.get_book_price);
                EditText book_press = (EditText) dialog.findViewById(R.id.get_book_Press);
                new_book.setName(book_name.getText().toString().trim());
                new_book.setAuthor(book_author.getText().toString().trim());
                new_book.setPages(Integer.valueOf(book_pages.getText().toString().trim()));
                new_book.setPrice(Double.parseDouble(book_price.getText().toString().trim()));
                new_book.setPress(book_press.getText().toString().trim());
                Log.d(TAG, "onClick: 存入book");
                Log.i(TAG, "onClick: new_book =" + new_book.toString());
                new_book.save();
                message = "确定";
                dialog.dismiss();
                dialog = new Dialog(this);
                dialog.setContentView(R.layout.add_book_data_dailog);
                Log.i(TAG, "onClick: " + dialog.toString());
                break;
            case R.id.edit_data_cancel:
                dialog.dismiss();
                Log.i(TAG, "onClick: edit_data_cancel");
                message = "取消";
                break;
            case R.id.select_all:
                Log.i(TAG, "onClick: select_all");
                message = "查询所有";

                break;
            case R.id.select_by_id:
                int select_id = 0;
                message = "根据id"+ select_id+"查询";
                break;
            default:
                message = "响应失败";
                Log.i(TAG, "onClick: default");
                break;
        }
        Toast.makeText(Main_Activity.this, message, Toast.LENGTH_SHORT).show();
    }
}

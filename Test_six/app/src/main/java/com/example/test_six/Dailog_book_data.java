package com.example.test_six;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

/**
 * Created by czm
 */

public class Dailog_book_data extends Dialog {
    private static final String TAG = "Dailog_book_data";
    private Context context;
    private String title;
    private EditText name;
    private EditText author;
    private EditText price;
    private EditText pages;

    /**
     * 构造函数
     *
     * @param context 对应的activity
     * @param title   提示信息
     */
    public Dailog_book_data(Context context, String title) {
        super(context);
        this.context = context;
        this.title = title;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public int inint() {
        //构造view
        Log.d(TAG, "inint: 构造view");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.add_book_data_dailog, null);
        setContentView(view);


        //获得dailog
        Log.d(TAG, "inint: 获得dailog");
        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.8); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
        return 1;
    }


    /**
     * 组装一个book
     *
     * @return book数据
     */
    private Book getBook() {
        Log.i(TAG, "getBook: 组装Book");
        Book book = new Book();
        String name_str = name.getText().toString().trim();
        if (name_str != "" && !name_str.equals(""))
            book.setName(name_str);

        return book;
    }
}

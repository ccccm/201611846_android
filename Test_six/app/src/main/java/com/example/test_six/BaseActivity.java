package com.example.test_six;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by czm
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity_Collector.addActivity(this);
        Activity_Collector.setActivity_now(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Activity_Collector.removeActivity(this);
    }
}

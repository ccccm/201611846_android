package com.example.test_four;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by czm
 */

/**
 * 广播另一个接收器
 */
public class AntherBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "AntherBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive: AntherBroadcastReceiver");
        Toast.makeText(context, "received in AnthorBroadcastReceiver", Toast.LENGTH_SHORT).show();
    }
}

package com.example.test_five;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by czm
 */

/**
 * activity控制器
 */
public class Activity_Collector {
    private static final String TAG = "Activity_Collector";
    public static List<Activity> activities = new ArrayList<>();
    public static Activity activity_now;

    public static void addActivity(Activity activity) {
        Log.d(TAG, "addActivity: activity_name:" + activity.toString());
        activity_now = activity;
        activities.add(activity);
    }

    public static void removeActivity(Activity activity) {
        Log.d(TAG, "removeActivity: activity_name: " + activity.toString());
        activities.remove(activity);
        ;
    }

    public static Activity getActivity_now() {
        return activity_now;
    }

    public static void setActivity_now(Activity activity_now) {
        Log.d(TAG, "setActivity_now: activity_name: " + activity_now);
        Activity_Collector.activity_now = activity_now;
    }

    public static void finishAll() {
        Log.i(TAG, "finishAll: finishall");
        for (Activity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
        activities.clear();
    }

    public static List<Activity> getActivities() {
        return activities;
    }

    public static void setActivities(List<Activity> activities) {
        Activity_Collector.activities = activities;
    }
}

package com.example.test_five;

import android.app.Activity;
import android.content.Intent;
import android.icu.util.ValueIterator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.force_offline:
                Log.i(TAG, "onClick: force_offline");
                Intent intent = new Intent("com.example.test_five.FORCE_OFFLINE");
                Log.i(TAG, "onClick: 发送广播: " + intent.getAction().toString());
                sendBroadcast(intent);
        }
    }
}

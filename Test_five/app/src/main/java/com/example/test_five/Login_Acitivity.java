package com.example.test_five;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

/**
 * 登录activity
 */
public class Login_Acitivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "Login_Acitivity";
    private EditText account_edit;
    private EditText password_edit;
    private String account_ture = "admin";
    private String password_true = "123456";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.d(TAG, "onCreate: findViewById");
        account_edit = (EditText) findViewById(R.id.account);
        password_edit = (EditText) findViewById(R.id.password);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                Log.i(TAG, "onClick: login");
                String account = account_edit.getText().toString().trim();
                String password = password_edit.getText().toString().trim();
                /*账号admin 密码123456*/
                if (account.equals(account_ture) && password.equals(password_true)) {
                    Log.i(TAG, "onClick: login： 验证成功");
                    Intent intent = new Intent(Login_Acitivity.this, MainActivity.class);
                    startActivity(intent);
                    /*结束activity*/
                    finish();
                    break;
                }
            default:
                Log.i(TAG, "onClick: default");
                break;
        }
    }
}

package com.example.test_five;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by czm
 */

/**
 * 作为activity的父类  实现控制器的数据传递
 */
public class BaseActivity extends AppCompatActivity {
    private ForceOfflineReceiver receiver;
    private static String TAG = "BaseActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity_Collector.addActivity(this);
        Activity_Collector.setActivity_now(this);
    }

    @Override
    /**
     * 保证栈顶注册接收器
     */
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: 动态注册");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.example.test_five.FORCE_OFFLINE");
        receiver = new ForceOfflineReceiver();
        registerReceiver(receiver, intentFilter);
    }

    @Override
    /**
     * 保证非栈顶不拥有接收器
     */
    protected void onPause() {
        super.onPause();
        if (receiver != null) {
            Log.i(TAG, "onPause: 注销" + receiver.toString());
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Activity_Collector.removeActivity(this);
    }

    /**
     * 广播接收器
     */
    class ForceOfflineReceiver extends BroadcastReceiver {
        private static final String TAG = "ForceOfflineReceiver";

        @Override
        public void onReceive(final Context context, Intent intent) {
            /*弹出对话框*/
            Log.d(TAG, "onReceive: 对话框配置");
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Warning");
            builder.setMessage("您已经被强制下线，请重新登录");
            builder.setCancelable(false);//设置为不可取消
            builder.setPositiveButton("好的", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Log.i(TAG, "onClick: finishAll");
                    Activity_Collector.finishAll();
                    Log.i(TAG, "onClick: 跳转往Login_Activity");
                    Intent intent = new Intent(context, Login_Acitivity.class);
                    context.startActivity(intent);
                }
            });
            Log.i(TAG, "onReceive: 弹出对话框");
            builder.show();
        }
    }
}

package com.example.test_ten;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";
    TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button sendRequest = (Button) findViewById(R.id.send_request);
        responseText = (TextView) findViewById(R.id.response_text);
        sendRequest.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.send_request) {
            send_requsetWithHttpOkHttp();
        }
    }

    private void send_requsetWithHttpOkHttp() {

        // 开启线程来发起网络请求
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "run: 新线程");
                try {
                    OkHttpClient client = new OkHttpClient();
                    // 建立一个request
                    URL url = new URL("http://www.baidu.com");
                    Log.i(TAG, "run: 建立一个关于OkHttp的request");
                    Request request = new Request.Builder().url(url).build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    showResponse(responseData);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void showResponse(final String response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //这里进行UI操作，将结果显示在界面上
                Log.i(TAG, "run: 显示结果");
                responseText.setText(response);
            }
        });
    }
}

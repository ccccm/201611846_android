package com.example.test_one;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
/**
 * Create by Czm
 */
public class Hello_Activity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "Hello_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);
        setTitle("Hello_Activity");
        ActivityControl.addActivities(Hello_Activity.this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.to_Hey:
                Log.i(TAG, "to_Hey");
                intent.setClass(Hello_Activity.this, Hey_Activity.class);
                startActivity(intent);
                break;
            case R.id.to_Hi:
                Log.i(TAG, "to_Hi");
                intent.setClass(Hello_Activity.this, Hi_Acitivty.class);
                startActivity(intent);
                break;
            case R.id.to_Hay:
                Log.i(TAG, "to_Hay");
                intent.setAction("com.example.test_one.ACTION_START");
                intent.addCategory("com.example.test_one.HAY_ACTIVITY");
                startActivity(intent);
                break;
            case R.id.close_all:
                Log.i(TAG, "close_all");
                //Toast.makeText(Hello_Activity.this, "close_all", Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder dialog = new AlertDialog.Builder(Hello_Activity.this);
                dialog.setMessage("是否结束所有活动");
                dialog.setCancelable(false);
                dialog.setNegativeButton("是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityControl.finishAll();
                    }
                });
                dialog.setNeutralButton("否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whcih) {
                        dialog.dismiss();
                    }
                });
                dialog.setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whcih) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.Progressdialog:
                Log.i(TAG,"Progrssdialog");
                ProgressDialog progressDialog = new ProgressDialog(Hello_Activity.this);
                progressDialog.setTitle("老流氓牛逼！！！");
                progressDialog.setMessage("老流氓正在思考算法...");
                progressDialog.setCancelable(true);
                progressDialog.show();
                break;
            default:
                Log.i(TAG, "defaul");
                break;
        }
    }

    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    protected void onDestroy() {
        ActivityControl.removeActivity(Hello_Activity.this);
        super.onDestroy();
        Log.i(TAG, "onDesroy");
    }
}

package com.example.test_one;

import android.app.Activity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * create by czm
 */

public class ActivityControl {
    private static final String TAG = "ActivityControl";
    private static List<Activity> activities = new ArrayList<>();

    public static void addActivities(Activity activity) {
        activities.add(activity);
        Log.i(TAG, "addAcitivityies");
    }

    public static void removeActivity(Activity activity) {
        activities.remove(activity);
        Log.i(TAG, "remveActivities");
    }

    public static void finishAll() {
        for (Activity activity : activities) {
            if (!activity.isFinishing()) {
                activity.finish();
            }
            activities.clear();
            Log.i(TAG, "clearactivities");
        }
    }
}

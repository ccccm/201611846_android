package com.example.test_one;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Hay_Activity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Hay_Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hay);
        Log.i(TAG, "onCreate");
        ActivityControl.addActivities(Hay_Activity.this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.to_Web:
                Log.i(TAG, "to_Web");
                intent.setAction(Intent.ACTION_VIEW);
                Toast.makeText(Hay_Activity.this, "to_Web", Toast.LENGTH_SHORT).show();
                intent.setData(Uri.parse("http://www.baidu.com"));
                startActivity(intent);
                break;
            case R.id.to_back:
                Log.i(TAG, "to_back");
                finish();
                break;
            case R.id.close_all:
                Log.i(TAG, "close_all");
                ActivityControl.finishAll();
                break;
            default:
                Log.i(TAG, "dafault");
                break;
        }
    }

    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    protected void onDestroy() {
        ActivityControl.removeActivity(Hay_Activity.this);
        super.onDestroy();
        Log.i(TAG, "onDesroy");
    }
}

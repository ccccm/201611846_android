package com.example.test_one;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

/**
 * Create by Czm
 */

public class Hi_Acitivty extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "Hi_Acitivty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hey);
        ActivityControl.addActivities(Hi_Acitivty.this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.to_Hello:
                Log.i(TAG, "to_Hello");
                intent.setClass(Hi_Acitivty.this, Hello_Activity.class);
                startActivity(intent);
                break;

            case R.id.to_Hey:
                Log.i(TAG, "to_Hey");
                intent.setClass(Hi_Acitivty.this, Hey_Activity.class);
                startActivity(intent);
                break;
            case R.id.close_all:
                Log.i(TAG, "close_all");
                ActivityControl.finishAll();
                break;
            default:
                Log.i(TAG, "default");
                break;
        }
    }

    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    protected void onDestroy() {
        ActivityControl.removeActivity(Hi_Acitivty.this);
        super.onDestroy();
        Log.i(TAG, "onDesroy");
    }
}

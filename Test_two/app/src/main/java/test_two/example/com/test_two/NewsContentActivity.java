package test_two.example.com.test_two;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class NewsContentActivity extends AppCompatActivity {
    private static final String TAG = "NewsContentActivity";

    /**
     * 启动activity
     *
     * @param context
     * @param newsTitle   新闻标题
     * @param newsContent 新闻主题
     */
    public static void actionStart(Context context, String newsTitle, String newsContent) {
        Log.i(TAG, "actionStart: 启动activity");
        Log.i(TAG, "actionStart: context= " + context.toString());
        Intent intent = new Intent(context, NewsContentActivity.class);
        //获取参数
        intent.putExtra("news_title", newsTitle);
        Log.i(TAG, "actionStart: newsTitle= " + newsTitle.toString());
        intent.putExtra("news_content", newsContent);
        Log.i(TAG, "actionStart: newsContent= " + newsContent.toString());
        context.startActivity(intent);
    }

    /**
     * activity开始
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_content);
        Log.i(TAG, "onCreate: 活动开始");
        String newsTitle = getIntent().getStringExtra("news_title");//获取传入的新闻标题
        Log.i(TAG, "onCreate: 获取传入的新闻标题: newsTitle= " + newsTitle);
        Log.i(TAG, "onCreate: 获取传入的新闻内容");
        String newsContent = getIntent().getStringExtra("news_content");//获取传入的新闻内容
        Log.i(TAG, "onCreate: 刷新 NewsContentFragment界面");
        NewsContentFragment newsContentFragment = (NewsContentFragment) getSupportFragmentManager().findFragmentById(R.id.news_content_fragment);
        newsContentFragment.refresh(newsTitle, newsContent);//刷新 NewsContentFragment 界面
    }


}

package test_two.example.com.test_two;

/**
 * Created by czm
 */


public class News {
    private String title;
    private String content;
    /**
     * @param title   新闻标题
     * @param content 新闻内容
     */
    public News(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public News() {
        super();
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package test_two.example.com.test_two;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by czm
 */

public class NewsContentFragment extends Fragment {
    private View view;
    private static final String TAG = "NewsContentFragment";

    /**
     * 加载布局
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return 加载完成的view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: 加载布局");
        view = inflater.inflate(R.layout.news_content_frag, container, false);
        return view;
    }

    /**
     * 将标题与正文显示在界面上
     *
     * @param newsTitle   新闻标题
     * @param newsContent 新闻正文
     */
    public void refresh(String newsTitle, String newsContent) {
        Log.d(TAG, "refresh: 刷新界面");
        //寻找view
        View visibilityLayout = view.findViewById(R.id.visibility_layout);
        //模式:显示
        //参考网址：https://www.cnblogs.com/bluestorm/p/3666345.html
        visibilityLayout.setVisibility(View.VISIBLE);
        TextView newsTitleText = (TextView) view.findViewById(R.id.news_title); //标题
        TextView newsContentText = (TextView) view.findViewById(R.id.news_content);//主体
        Log.i(TAG, "refresh: " + newsTitle.toString());
        newsTitleText.setText(newsTitle); //组装标题
        Log.i(TAG, "refresh: " + newsContent.toString());
        newsContentText.setText(newsContent); // 组装主体
    }
}

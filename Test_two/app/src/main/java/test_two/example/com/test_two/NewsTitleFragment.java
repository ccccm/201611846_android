package test_two.example.com.test_two;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by czm
 */

public class NewsTitleFragment extends Fragment {
    private boolean isTwoPane; //是否双页开关
    private static final String TAG = "NewsTitleFragment";

    class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
        private List<News> mNewsList;

        public NewsAdapter(List<News> NewsList) {
            mNewsList = NewsList;
        }

        /**
         * 全局更新
         *
         * @param parent
         * @param viewType
         * @return
         */
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);
            Log.i(TAG, "onCreateViewHolder: 装配holder");
            final ViewHolder holder = new ViewHolder(view); // 装配holder
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    News news = mNewsList.get(holder.getAdapterPosition());
                    if (isTwoPane) {
                        //如果是双页模式，就刷新NewsContentFragment中的内容
                        Log.i(TAG, "onClick: 双页模式");
                        NewsContentFragment newsContentFragment = (NewsContentFragment) getFragmentManager().findFragmentById(R.id.news_content_fragment);
                        newsContentFragment.refresh(news.getTitle(), news.getContent());
                    } else {
                        //如果是单页模式，则直接启动NewsContentActivity
                        Log.i(TAG, "onClick: 单页模式");
                        NewsContentActivity.actionStart(getActivity(), news.getTitle(), news.getContent());
                    }
                }
            });
            Log.i(TAG, "onCreateViewHolder: 返回holder= " + holder.toString());
            return holder;
        }

        /**
         * 局部更新
         *
         * @param holder   对应的holder用于加载
         * @param position 位置
         */
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Log.i(TAG, "onBindViewHolder: 局部更新" + position + "处的信息");
            News news = mNewsList.get(position);
            Log.i(TAG, "onBindViewHolder: position= " + position);
            holder.newsTitleText.setText(news.getTitle());
        }

        /**
         * 获得长度
         *
         * @return 列表长度
         */
        @Override
        public int getItemCount() {
            Log.i(TAG, "getItemCount: mNewsList.szie= " + mNewsList.size());
            return mNewsList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView newsTitleText;

            /**
             * 构造函数
             *
             * @param view
             */
            public ViewHolder(View view) {
                super(view);
                Log.i(TAG, "ViewHolder: 构造函数");
                newsTitleText = (TextView) view.findViewById(R.id.news_title);
                Log.i(TAG, "ViewHolder: newsTitleText= " + newsTitleText.toString());
            }
        }
    }

    /**
     * 获得view函数
     *
     * @param inflater
     * @param container
     * @param saveInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.news_title_frag, container, false);
        //初始化recyclerview
        Log.i(TAG, "onCreateView: 初始化Recyclerview");
        RecyclerView newsTitleRecyclerView = (RecyclerView) view.findViewById(R.id.news_title_recyclter_view);
        /*这里使用了getActivity是因为碎片需要获得当前activity的信息*/
        Log.i(TAG, "onCreateView: 获得信息= " + getActivity().getClass().toString());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        Log.i(TAG, "onCreateView: 配置layoutManager");
        newsTitleRecyclerView.setLayoutManager(layoutManager);
        /*适配器*/
        Log.i(TAG, "onCreateView: 建立适配器");
        NewsAdapter adapter = new NewsAdapter(getNews());
        Log.i(TAG, "onCreateView: 配置适配器");
        newsTitleRecyclerView.setAdapter(adapter);
        Log.i(TAG, "onCreateView: 返回view");
        return view;
    }

    /**
     * 加载news_title_frag 布局
     *
     * @param saveInstanceState
     */
    @SuppressLint("MissingSuperCall")
    @Override
    public void onActivityCreated(Bundle saveInstanceState) {
        super.onActivityCreated(saveInstanceState);
        if (getActivity().findViewById(R.id.news_content_layout) != null) {
            Log.i(TAG, "onActivityCreated: 双页模式");
            isTwoPane = true;//可以找到news_content_layout布局时，为双页模式
        } else {
            Log.i(TAG, "onActivityCreated: 单页模式");
            isTwoPane = false; //不可以找到news_content_layout布局时，为单页模式
        }
    }

    /**
     * 生成新闻
     *
     * @return 新闻列表
     */
    private List<News> getNews() {
        List<News> newsList = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            News news = new News();
            news.setTitle("这个是第" + i + "条新闻");
            news.setContent(getRandomLengthContent("This is news content" + i + "."));
            Log.i(TAG, "getNews: 第" + i + "条新闻生成");
            newsList.add(news);
        }
        Log.d(TAG, "getNews: 返回新闻列表");
        return newsList;
    }

    /**
     * 生成新闻主体
     *
     * @param content 字符串用于复制
     * @return 一条新闻主体
     */
    private String getRandomLengthContent(String content) {
        Log.i(TAG, "getRandomLengthContent: 获得随机新闻主体");
        Random random = new Random();
        int length = random.nextInt(20) + 1;
        StringBuilder builer = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builer.append(content);
        }
        return builer.toString();
    }
}
